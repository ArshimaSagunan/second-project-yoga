from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from program.models import program,Category,order
from client.models import Appoinment,User
# Create your views here.
import razorpay,datetime
client =  razorpay.Client(auth = ("rzp_test_XgmvlQJpfvtHMI","czQviVvBYLM5z22tRgO21J6U"))

@login_required
def create_order(request):
	context = {}
	if request.method =='POST':
		catId = request.POST.get("category")
		cat = Category.objects.get(id = catId)
		category_name = cat.category
		amount = cat.price
		
		user = request.user
		name = user.get_full_name()
		phone = user.phone
		email = user.email
		order_amount = int(amount)*100
		notes = {'Shipping address': user.address}
		order_currency ="INR"

		order_receipt = datetime.date.today().strftime('%D')
		start_date = datetime.date.today().strftime('%Y-%m-%d')
	
		# CREAING ORDER
		response = client.order.create(dict(amount=order_amount, currency=order_currency, receipt=order_receipt, notes=notes, payment_capture='1'))
		order_id = response['id']
		order_status = response['status']

		if order_status=='created':

		# Server data for user convinience
			context['product_id'] = order_id
			context['price'] = order_amount
			context['name'] = name
			context['phone'] = phone
			context['email'] = email
			# data that'll be send to the razorpay for
			context['order_id'] = order_id
			context['product_price'] = amount
			context['catId'] = catId
			context['category_name'] = category_name
			context['start_date'] = start_date
				
	return render(request,'payment/confirm-page.html', context)

def payment_status(request):

	response = request.POST
	print("----------- response",response,"-----------")
	date = request.POST.get('date')
	time = request.POST.get('time')
	categoryid = request.POST.get('categoryid')
	amount = int(request.POST.get('price'))
	user = request.user
	
	params_dict = {
	'razorpay_payment_id' : response['razorpay_payment_id'],
	'razorpay_order_id' : response['razorpay_order_id'],
	'razorpay_signature' : response['razorpay_signature']
	}
	
	try:
		status = client.utility.verify_payment_signature(params_dict)
		order_id =response['razorpay_order_id']
		payment_id = response['razorpay_payment_id']
		signature = response['razorpay_signature']
		categoryid = Category.objects.get(id = categoryid)
		order.objects.create(order_id=order_id,payment_id=payment_id,categoryid=categoryid,signature=signature,userid=request.user,amount=amount,date=date,time=time)
	except:
		return render(request, 'payment/order_summary.html', {'status': 'Payment Faliure!!!'})
	else:
		user.status = "Subscriber"
		user.save()
		# return HttpResponse("Payment success")
		k=program.objects.filter(category__id=categoryid.id)
		cat = Category.objects.get(id = categoryid.id)
		bought = False
		total_pgm = k.count()
		if request.user.is_authenticated:
		    user = request.user
		    if order.objects.filter(userid = user,categoryid = cat).exists():
		        bought = True
		total_pgm = k.count() 
		extra = {'popup':True,'heading':'Payment Success','msg':'You have successfully bought the Package'}
		return render(request,"home/program.html",{'k':k,'total_pgm':total_pgm,'cat':cat,'extra':extra,'is_bought':bought})



@login_required
def create_appoinment(request):
	context = {}
	print("hello")
	if request.method =='POST':
		appoinId = request.POST.get("appoinment")
		print(appoinId)
		appoinment = Appoinment.objects.get(id = appoinId)
		appoin_date = appoinment.appoinmentdate
		print(appoin_date)
		doctor_name = appoinment.doctor
		print(doctor_name)
		depart_name = appoinment.department
		print(depart_name)
		name = appoinment.name
		print(name)
		email = appoinment.email
		phone = appoinment.phone
		amount = 100
		order_amount = amount*100
		notes = appoinment.address
		order_currency ="INR"
		order_receipt = "24/12/2020"
		start_date = datetime.date.today().strftime('%Y-%m-%d')
		# CREAING ORDER
		response = client.order.create(dict(amount=order_amount, currency=order_currency, receipt=order_receipt, notes=notes, payment_capture='1'))
		order_id = response['id']
		order_status = response['status']

		if order_status=='created':

		# Server data for user convinience
			context['product_id'] = order_id
			context['price'] = order_amount
			context['name'] = name
			context['phone'] = phone
			context['email'] = email
			# data that'll be send to the razorpay for
			context['order_id'] = order_id
			context['product_price'] = amount
			context['appoinId'] = appoinId
			context['appoin_date'] = appoin_date
			context['doctor_name'] = doctor_name
			context['depart_name'] = depart_name
			context['start_date'] = start_date
			
	
	return render(request,'home/confirm-appoinment.html', context)


def appoinment_status(request):
    
	response = request.POST
	print("----------- response",response,"-----------")
	appointmentId = request.POST.get('appointmentId')
	
	user = request.user
	
	params_dict = {
	'razorpay_payment_id' : response['razorpay_payment_id'],
	'razorpay_order_id' : response['razorpay_order_id'],
	'razorpay_signature' : response['razorpay_signature']
	}
	
	try:
		status = client.utility.verify_payment_signature(params_dict)
		order_id =response['razorpay_order_id']
		payment_id = response['razorpay_payment_id']
		signature = response['razorpay_signature']
		appointment = Appoinment.objects.get(id = appointmentId)

		appointment.payment_id = payment_id
		appointment.order_id  = order_id
		appointment.signature = signature
		appointment.save()
		
	except:
		return render(request, 'payment/order_summary.html', {'status': 'Payment Faliure!!!'})
	else:
		extra = {'popup':True,'heading':'Appoinment Made !!','msg':'Your appointment with doctor '+str(appointment.doctor.doctor.upper())+' is made successfully.     Appointment date: '+str(appointment.appoinmentdate)}
		return render(request,'home/appoinment.html', {'extra':extra})
